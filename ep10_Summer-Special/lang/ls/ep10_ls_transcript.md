# Transcript of Pepper&Carrot Episode 10 [ls]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Título|1|False|Episodio 10: Especial de verano

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrador|1|False|- FIN -
Créditos|2|False|08/2015 - Art & Scenario : David Revoy - Translation: Alex Gryson

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Créditos|1|False|Pepper&Carrot es totalmente gratuito, de código abierto y patrocinado gracias a nuestros lectores. Para este episodio, muchas gracias a nuestros 422 mecenas:
Créditos|3|False|https://www.patreon.com/davidrevoy
Créditos|2|True|Tú también puedes convertirte en mecenas del siguiente episodio de Pepper&Carrot en:
Créditos|4|False|Licencia: Creative Commons Atribución 4.0. Archivos fuente: disponibles en www.peppercarrot.com Software: este episodio fue realizado con el software 100% libre Krita 2.9.6, Inkscape 0.91 en Linux Mint 17.
