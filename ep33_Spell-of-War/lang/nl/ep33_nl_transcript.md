# Transcript of Pepper&Carrot Episode 33 [nl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Aflevering 33: De Ban des Oorlogs

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Enemy|1|False|ahWOEEEEEEEEEE !
Army|2|False|Roooooar !!
Army|5|False|Grroooaarr !!
Army|4|False|Grrrr !
Army|3|False|Yuurrr !

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
King|1|True|Oké, jonge heks.
King|2|False|Als je een spreuk kent die ons helpen kan, dan is het nu of nooit!
Pepper|3|True|Begrepen!
Pepper|4|False|Zet U schrap en aanschouw...
Sound|5|False|Dzjiii! !|nowhitespace
Pepper|6|False|...MIJN MEESTERWERK!!
Sound|7|False|Dzjiiii! !|nowhitespace
Pepper|8|False|Realitas Hackeris Pepperus!
Sound|9|False|Dzzjooo! !|nowhitespace

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|False|Fiitz!
Sound|2|False|Dzii!
Sound|3|False|Tshii!
Sound|4|False|Ffhii!
Sound|8|False|Dziing!
Sound|7|False|Fiitz!
Sound|6|False|Tshii!
Sound|5|False|Ffhii!
King|9|True|Is dit een spreuk die onze zwaarden sterker maakt...
King|10|False|...en de wapens van onze vijanden zwakker?
Sound|11|False|Dzii...
Pepper|12|True|Hehe.
Pepper|13|True|U zult het zien!
Pepper|14|False|Het enige wat ik kan zeggen, is dat U vandaag geen enkele soldaat verliest.
Pepper|15|False|Maar niet zonder slag of stoot, en niet zonder jullie volle inzet!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
King|1|False|Dat is precies waarvoor we hebben geoefend!
King|2|False|OP MIJN BEVEL!
Army|3|False|Jaaa!
Army|4|False|Waaah!
Army|5|False|Jaaa!
Army|6|False|Jaaa!
Army|7|False|Waaaah!
Army|8|False|Jaaah!
Army|9|False|Yihaaa !
Army|10|False|Jahii!
Army|11|False|Jaa!
King|12|False|Aaaaaanvalluuuuuuuuh!!!
Army|13|False|Waaaaah!
Army|14|False|Jaaa!
Army|15|False|Yihaaa!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
King|1|False|Jaaaa!!
Enemy|2|False|Whaaah!!!
Sound|3|False|Tshiiiing! !|nowhitespace
Sound|4|False|Swoosh! !|nowhitespace
Writing|5|False|12
Enemy|6|False|?!!
Writing|7|False|8
King|8|False|?!!
Writing|9|False|64
Writing|10|False|32
Writing|11|False|72
Writing|12|False|0
Writing|13|False|64
Writing|14|False|0
Writing|15|False|56
Army|20|False|Yrrr !
Army|17|False|Yurr !
Army|19|False|Grrr !
Army|21|False|Yaaa !
Army|18|False|Ouaaais !
Army|16|False|Yâaa !
Sound|27|False|Sshing
Sound|25|False|Fffchh
Sound|22|False|wiizz
Sound|23|False|Swoosh
Sound|24|False|Chklong
Sound|26|False|Chkilng

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
King|1|False|?!
Writing|2|False|3
Writing|3|False|24
Writing|4|False|38
Writing|5|False|6
Writing|6|False|12
Writing|7|False|0
Writing|8|False|5
Writing|9|False|0
Writing|10|False|37
Writing|11|False|21
Writing|12|False|62
Writing|13|False|27
Writing|14|False|4
King|15|False|! !|nowhitespace
King|16|False|HÈÈÈÈÈKS! WAT HEB JIJ UITGESPOOKT?!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Tadaaa!
Pepper|2|True|"Dit"
Pepper|3|False|...is mijn meesterwerk!
Pepper|4|False|Het is een complexe bezwering die de realiteit verandert en het aantal overgebleven levenspunten van je tegenstander laat zien.
Writing|5|False|33
Pepper|6|True|Wanneer je levenspunten op zijn, moet je het slagveld verlaten tot het einde van het gevecht.
Pepper|7|True|Het eerste leger dat alle tegenstanders van het veld verwijdert, wint!
Pepper|8|False|Doodsimpel.
Writing|9|False|0
Army|10|False|?
Pepper|11|True|Geweldig toch, of niet?
Pepper|12|True|Geen doden meer!
Pepper|13|True|Geen gewonde soldaten meer!
Pepper|14|False|Dit gaat oorlogsvoering revolutioneren!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Nu dat jullie de regels kennen, zal ik alle tellers resetten en dan kunnen we overnieuw beginnen.
Pepper|2|False|Dat is wel zo eerlijk.
Writing|3|False|64
Writing|4|False|45
Writing|5|False|6
Writing|6|False|2
Writing|7|False|0
Writing|8|False|0
Writing|9|False|0
Writing|10|False|0
Writing|11|False|9
Writing|12|False|5
Writing|13|False|0
Writing|14|False|0
Writing|15|False|0
Sound|16|False|Sshing!
Sound|17|False|Zouu!
Sound|19|False|Tjak!
Sound|18|False|Bonk!
Pepper|20|True|Ren sneller, Carrot!
Pepper|21|False|De spreuk zal niet lang meer blijven werken!
Narrator|22|False|- Einde -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|1|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|2|False|You can also translate this page if you want.
<hidden>|3|False|Beta readers help with the story, proofreaders give feedback about the text.
Credits|4|False|29 juni 2020 Tekeningen & verhaal: David Revoy. Bèta-feedback: Arlo James Barnes, Craig Maloney, GunChleoc, Hyperbolic Pain, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Quetzal2, Valvin. Nederlandse versie Vertaling: Julien Bertholon. Gebaseerd op het Hereva-universum Bedenker: David Revoy. Hoofdonderhouder: Craig Maloney. Schrijvers: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Verbeteraars: Willem Sonke, Moini, Hali, CGand, Alex Gryson . Software: Krita 4.3, Inkscape 1.0 op Kubuntu 19.10. Licentie : Creative Commons Naamsvermelding 4.0. www.peppercarrot.com
Pepper|5|True|Wist je dat?
Pepper|6|True|Pepper&Carrot is helemaal vrij,open-bron en gesponsord door de giften van de lezers.
Pepper|7|False|Voor deze aflevering bedank ik de 1190 sponsors!
Pepper|8|True|Jij kan ook sponsor van Pepper & Carrot worden en dan komt jouw naam hierbij!
Pepper|9|True|Je kan doneren met Patreon, Tipeee, PayPal, Liberapay ... en meer!
Pepper|10|True|Kijk op www.peppercarrot.com voor alle info!
Pepper|11|False|Dank je!
