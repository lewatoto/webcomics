# Transcript of Pepper&Carrot Episode 04 [ls]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Título|1|False|Episodio 4: Golpe de genialidad

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrador|1|False|La noche anterior al concurso de pociones, 2:00 AM.
Pepper|2|True|Finalmente... Con esta poción, nadie, ni siquiera Saffron, ¡podrá igualar mi habilidad!
Pepper|3|False|Necesito probarla en alguien ...
Pepper|4|False|¡¡¡CAAAAAAAA~ ~AAARROT!!!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Texto|14|False|COMIDA PARA GATO
Pepper|1|False|¿Carrot?
Pepper|2|False|¡Increíble! ¿Ni siquiera está bajo la cama?
Pepper|4|False|¿Carrot?
Pepper|5|False|¡¿Carrot?!
Pepper|6|False|¡¡¡¿Carrot?!!!
Pepper|3|False|¡Carrot!
Pepper|7|False|Realmente no pensé que necesitaría usar este truco...
Pepper|8|False|¡Carrot!
Sonido|9|True|Crrr
Sonido|10|True|Crrr
Sonido|11|True|Crrr
Sonido|12|True|Crrr
Sonido|13|False|Crrr
Sonido|15|True|Shhh
Sonido|16|True|Shhh
Sonido|17|True|Shhh
Sonido|18|True|Shhh
Sonido|19|False|Shhh
Sonido|21|False|Shhh Shhh Shhh Shhh Shhh
Sonido|20|False|Crrr Crrr Crrr Crrr Crrr
Sonido|23|False|Crrr Crrr Crrr Crrr Crrr
Sonido|24|False|Shhh Shhh Shhh Shhh Shhh
Carrot|25|False|Grooo
Sonido|26|False|Pffioooo !
Carrot|22|False|O Sole Meowww !

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Awww, ¡Carrot! tú dulce gatito has venido por tu cuenta.
Pepper|2|False|Justo en el momento en que necesito a mi asistente favorito.
Pepper|3|True|Aquí tienes, te presento mi obra maestra.
Pepper|4|False|¡La poción del Genio!
Pepper|5|False|Por favor, toma un sorbo.
Pepper|6|False|Y si funciona, deberías tener un golpe de genialidad.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonido|1|False|Poc
Pepper|3|False|WOW... ¡Carrot! es increíble... ¿una letra? ...
Pepper|5|False|Tú... Tú ¿estás escribiendo?
Pepper|6|False|¿E?
Pepper|7|False|¿Energía?
Pepper|8|False|¿Eterno?
Pepper|9|False|¿Emoción?
Sonido|4|False|Shrrrrrrr
Sonido|2|False|Shrr

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|¿Qué más puede significar esto?
Pepper|2|False|¿Encajar?
Pepper|3|False|¿Empírico?
Pepper|4|False|¡Vamos Carrot! Me estoy quedando sin palabras, ¡esto no tiene ningún sentido!
Pepper|5|False|...esto no tiene ningún sentido...
Pepper|6|True|¡¡¡Grrrr...!!!
Pepper|7|False|¡Nunca me sale nada bien!
Pepper|8|True|Bah...
Pepper|9|False|Aún tengo tiempo para inventar otra poción.
Sonido|10|False|CR...A...CKKKK !
Pepper|11|False|Buenas noches Carrot.
Narrador|12|False|Continuará...
Créditos|13|False|David Revoy 11/2014

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Créditos|3|False|Алексей - 獨孤欣 - Adrian Lord - Alan Hardman - Albert Westra - Alejandro Flores Prieto - Alexander Sopicki - Alex Lusco Alex Silver - Alex Vandiver - Alfredo - Alice Queenofcats - Ali Poulton (Aunty Pol) - Alison Harding - Allan Zieser Andreas Rieger - Andrew - Andrew Godfrey - Andrey Alekseenko - Angela K - Anna Orlova - anonymous - Antan Karmola Arco - Ardash Crowfoot - Arne Brix - Aslak Kjølås-Sæverud - Axel Bordelon - Axel Philipsenburg - barbix - Ben Evans Bernd - Boonsak Watanavisit - Boudewijn Rempt - Brian Behnke - carlos levischi - Charlotte Lacombe-bar - Chris Radcliff Chris Sakkas - Christophe Carré - Clara Dexter - Colby Driedger - Conway Scott Smith - Cyrille Largillier - Cyril Paciullo Daniel - david - Damien de Lemeny - Dmitry - Donald Hayward - Dubema Ulo - Eitan Goldshtrom - Enrico Billich-epsilon--Eric Schulz - Garret Patterson - Guillaume - Gustav Strömbom - Guy Davis - Happy Mimic - Helmar Suschka Ilyas Akhmedov - Inga Huang - Irene C. - Ivan Korotkov - Jared Tritsch - JDB - Jean-Baptiste Hebbrecht - Jessey Wright John - Jónatan Nilsson - Jon Brake - Joseph Bowman - Jurgo van den Elzen - Justin Diehl - Kai-Ting (Danil) Ko - Kasper Hansen Kathryn Wuerstl - Ken Mingyuan Xia - Liselle - Lorentz Grip - MacCoy - Magnus Kronnäs - Mandy - marcus - Martin Owens Masked Admirer - Mathias Stærk - mattscribbles - Maurice-Marie Stromer - mefflin ross bullis-bates - Michael Gill Michelle Pereira Garcia - Mike Mosher - Morten Hellesø Johansen - Muzyka Dmytro - Nazhif - Nicholas DeLateur Nick Allott - Nicki Aya - Nicolae Berbece - Nicole Heersema - Noah Summers - Noble Hays - Nora Czaykowski Nyx - Olivier Amrein - Olivier Brun - Omar Willey - Oscar Moreno - Öykü Su Gürler - Ozone S. - Paul - Paul Pavel Semenov - Peter - Peter Moonen - Petr Vlašic - Pierre Geier - Pierre Vuillemin - Pranab Shenoy - Rajul Gupta Ret Samys - rictic - RJ van der Weide - Roman - Rumiko Hoshino - Rustin Simons - Sami T - Samuel Mitson - Scott RussSean Adams - Shadefalcon - Shane Lopez - Shawn Meyer - Sigmundur Þórir Jónsson - Simon Forster - Simon IsenbergSonny W. - Stanislav Vodetskyi - Stephen Bates - Steven Bennett - Stuart Dickson - surt - Tadamichi Ohkubo - tar8156TheFaico - Thomas Schwery - Tracey Reuben - Travis Humble - Vespertinus - Victoria - Vlad Tomash - Westen CurryXavier Claude - Yalyn Vinkindo y ¡agradecimientos especiales a Amireeti por apoyarme con las correcciones en inglés!
Créditos|1|False|Pepper&Carrot es totalmente gratuito, de código abierto y patrocinado con la ayuda de mecenas. Este episodio fue patrocinado por 156 mecenas:
Créditos|6|False|https://www.patreon.com/davidrevoy
Créditos|5|False|Apoya al proyecto, ¡una donación de un dólar por episodio, puede ayudar mucho a Pepper&Carrot!
Créditos|9|False|Herramientas: este episodio fue realizado con herramientas libres y de código abierto Krita, G'MIC, Blender, GIMP en Ubuntu Gnome (GNU/Linux)
Créditos|8|False|Código abierto: archivos en alta resolución para impresión, con tipografías disponibles para descargar, vender, modificar, traducir, etc...
Créditos|7|False|Licencia : Creative Commons Atribución a 'David Revoy' puedes hacer trabajos derivados, modificaciones, resubirlo, venderlo, etc...
Carrot|4|False|¡Gracias!
