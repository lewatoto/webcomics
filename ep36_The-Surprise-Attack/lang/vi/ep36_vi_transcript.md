# Transcript of Pepper&Carrot Episode 36 [vi]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Tập 36: Cuộc Tấn Công Bất Ngờ

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Wasabi|1|True|XIN LỖI?
Wasabi|2|False|Mày nói đua!
Wasabi|3|False|NHỐT NHỎ TÀO LAO NÀY VÀ TÙ!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|False|Phí ch h...
Hạt Tiêu|2|True|Bực mình!
Hạt Tiêu|3|False|Tôi không thể làm gì ở trong này!
Hạt Tiêu|4|False|Nhà giam chống phép thuật! Gừừừ!
Sound|5|False|KHÀNG!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Hạt Tiêu|1|False|Sao mình qúa khờ khạo!
Thất Vị|2|True|Xiiiii, Hạt Tiêu!
Thất Vị|3|False|Im đi.
Hạt Tiêu|4|False|Ai ở đó?!
Thất Vị|5|True|Im! Im lặng!
Thất Vị|6|True|Bên đây
Thất Vị|7|False|Em sẽ giải cứu chị thoát khỏi chỗ này.
Sound|8|False|Síííí...
Hạt Tiêu|9|False|Thất Vị hả?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Thất Vị|1|True|Em... Em xin lỗi những điều gì đã xảy ra...
Thất Vị|2|True|...và cuộc chiến đấu của chúng tôi.
Thất Vị|3|True|Em...
Thất Vị|4|False|Em phải làm vậy.
Hạt Tiêu|5|True|Đừng lo - chị biết.
Hạt Tiêu|6|False|Cám ơn em đã đến.
Thất Vị|7|False|Phòng chống phép thuật này rất mạnh mẽ – họ đã dùng hết sức để hạn chế chị!
Hạt Tiêu|8|False|Hà hà!
Thất Vị|9|False|Im lặng , họ sẽ nghe tiếng của chúng tôi.
Rat|10|True|LIẾM
Rat|11|True|LIẾM
Rat|12|False|LIẾM
Thất Vị|13|True|Chị biết không?
Thất Vị|14|True|Một lý do khác em tới là em được nhập vào nhóm nội bộ của Bà Cải Ngựa ngay sau nghi lễ.
Thất Vị|15|False|Và được nghe kế hoạch bí mật của bà ấy...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Thất Vị|6|True|KHÁNG !
Thất Vị|7|True|KHÓNG!
Thất Vị|8|False|KHÁNG !
Hạt Tiêu|9|True|KHỤNG !
Hạt Tiêu|10|True|KHÁNG !
Hạt Tiêu|11|False|Kế hoạch rất ác.
Thất Vị|12|True|Bà Cải Ngựa muốn thống trị tất cả các trường phép thuật, rất đơn giản...
Thất Vị|13|True|Sáng này bà ấy sẽ dẫn một lực lượng phù thủy quân sự đến Qủa Lý Thành...
Thất Vị|14|False|Ồ, không thể!
Thất Vị|15|False|Chị Cây Rau Mùi!
Sound|16|False|Và vương quốc của chị ấy!
Hạt Tiêu|17|True|Và phép thuật văn hóa Qũy Thân An!
Hạt Tiêu|18|False|Chúng tôi phải báo cáo liên!
Rat|19|False|Một phi công và con rồng chờ đợi mình trên sân ga để bay thoát khỏi nơi này.
Củ Đỏ|20|False|Rồi, cuối cùng ổ khóa đã vỡ!
Rat|21|False|Khàt!
Sound|1|True|Hoan hô!
Sound|2|True|Chị đi lấy Củ Đỏ và cái nón của chị.
Sound|3|True|chít!
Sound|4|True|Xìììì!
Sound|5|False|chít!
Hạt Tiêu|22|False|! ! !
Thất Vị|23|False|! ! !
Guard|24|True|NGƯỜI BẢO VỆ!
Guard|25|True|TỚI NGAY!
Guard|26|False|MỘt NGƯỜI XÂM NHẬP ĐANG THẢ NGƯỜI TÙ!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Thất Vị|1|False|Chúng tôi rất gần thoát...
Hạt Tiêu|2|False|Ơi, rất gần.
Hạt Tiêu|3|False|Ề, em biết tại sao Bà Cải Ngựa thù hận chị không??
Thất Vị|4|True|Bà ấy sợ phù thủy Châu Sa lắm, Chị Hạt Tiêu...
Thất Vị|5|True|Nhiều nhất là những chuỗi phản ứng của chị.
Thất Vị|6|False|Bà ấy tin tưởng chúng nó đe dọa kế hoạch của bà ấy rất lớn.
Hạt Tiêu|7|True|Ồ, cái đó...
Hạt Tiêu|8|False|Bà ấy chẳng cần lo gì từ cái đó ; chị chưa bao giờ thành công kích hoạt cái đó.
Thất Vị|9|False|Thật không??
Hạt Tiêu|10|False|Thật, hà hà hà!
Thất Vị|11|True|Hà-hà!
Thất Vị|12|False|Bà ấy qúa lo sợ...

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
King|1|True|Sĩ quan!
King|2|False|Ông thừa nhận đây là chùa của phù thủy đó không?
Officer|3|True|Xác suất rất cao, chỉ huy!
Officer|4|False|Vài gián điệp của chúng tôi mới xác minh phù thủy đó đang ở đây.
King|5|True|Ừừừừ ...
King|6|False|Thì ra là vậy, cô phù thủy đang đe dọa công nghệ và truyền thống chiến sĩ của chúng tôi đang ở đây!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
King|1|False|Hãy kết hợp liên minh của chúng tôi bằng cùng tay trả thù và phá hủy chùa này ra khỏi bề mặt của Hà Nhật Hạ
Sound|2|False|Páá
Enemy|3|False|Đồng ý!
Army|4|True|Yeah!
Army|5|True|Yeah!
Army|6|True|Yeah!
Army|7|True|Yeah!
Army|8|True|Yeehaaa!
Army|9|True|Yarr!
Army|10|True|Yeehaw !
Army|11|True|Yeee-haw!
Army|12|True|Yaaa!
Army|13|False|Hurrah!
King|14|True|MÁY PHÓNG!
King|15|False|BẮN!!!
Sound|16|False|húÚHÙÙÙÙÙÙÙÙÙÙ !
Sound|17|True|Pháááát!
Sound|18|False|Phháááát!
King|19|False|TẤẤẤN CÔÔÔNG!!!
Hạt Tiêu|20|True|Cái gì đang xảy ra?
Hạt Tiêu|21|False|Một cuộc tấn công không?!
Sound|22|True|PH ẠẠ NH!
Sound|23|False|PH Ạ ẠẠ NH!
Thất Vị|24|False|Cái gì?!
Sound|25|True|PHẠẠ ~ ẠẠẠ NH !
Sound|26|False|PHẠNH !

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Hạt Tiêu|1|True|ho
Hạt Tiêu|2|False|ho!!
Hạt Tiêu|3|False|Thất Vị có sao không?
Thất Vị|4|True|Dạ, em không sao!
Thất Vị|5|True|Còn chị?
Thất Vị|6|False|Và Củ Đỏ?
Hạt Tiêu|7|False|Chúng tôi không sao.
Hạt Tiêu|8|True|Cái gì đây...
Hạt Tiêu|9|False|Chị... không thể... tin được...
Thất Vị|10|True|Những lính đó từ đâu mà đến?!
Thất Vị|11|True|Và có máy phóng?!
Thất Vị|12|False|Họ muốn cái gì?
Hạt Tiêu|13|True|Chị không biết...
Hạt Tiêu|14|False|Nhưng chị có quen người đó.
Thất Vị|15|False|?
Thất Vị|16|False|! ! !
Thất Vị|17|False|Thổ Rễ Nha, qua đây!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|True|Thất Vị ơi!
Torreya|2|False|Cám ơn trời đất anh còn khỏe và an toàn.
Sound|3|False|Dính
Torreya|4|True|Em rất lo lắng sau khi em nghe họ đã nhốt anh trong tù!
Torreya|5|True|Và cuộc chiến đấu này!
Torreya|6|False|Hỗn loạn lắm!
Thất Vị|7|False|Thổ Rễ Nha, anh rất mừng được gặp lại em.
Hạt Tiêu|8|True|Ôi trời...
Hạt Tiêu|9|True|Phi công con rồng này là người tình yêu của Thất Vị...
Hạt Tiêu|10|True|Tôi không thể nghĩ đến hậu qủa nếu tôi đã hủy diệt nó.
Hạt Tiêu|11|True|Và chắc chắn hai quân đội đó đã theo dõi tôi.
Hạt Tiêu|12|True|Nếu họ không đến, chắc tôi vẫn bị nhốt trong tù .
Hạt Tiêu|13|False|Mỗi thứ có liên kết, bao ứng...
Hạt Tiêu|14|False|...Ồ!

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|Chị ấy bị gì vậy?
Thất Vị|2|False|Chị Hạ Tiêu? Có chuyện gì không??
Hạt Tiêu|3|True|Ừ, Chị không sao.
Hạt Tiêu|4|False|Chị mới phát hiện một điều.
Hạt Tiêu|5|True|Mỗi thứ đã xảy ra, trực tiếp hay gián tiếp, là kết qủa từ hành động và sự lựa chọn của chị...
Hạt Tiêu|6|False|...nói kiểu khác, chuỗi phản ứng của chị!
Thất Vị|7|True|Thật không?
Thất Vị|8|False|Chị cần giải thích cho em nghe.
Torreya|9|True|Nói chuyện đủ rồi, chúng tôi đang ở giữa một chiến trường.
Torreya|10|True|Chị có thể kể cho mình nghe khi đang bay an toàn trên trời.
Torreya|11|False|Leo lên Chị Hạt Tiêu!
Thất Vị|12|True|Thổ Rễ Nha nói đúng.
Thất Vị|13|False|Chúng tôi cần đi báo cảo cho Qủa Lý Thành ngay lập tức.
Hạt Tiêu|14|False|Chờ một chút nhe.
Hạt Tiêu|15|True|Quân đội của Bà Cải Ngựa đang chuẩn bị tấn công lại.
Hạt Tiêu|16|True|Chúng tôi không thể nào cho họ giết nhau như vậy.
Hạt Tiêu|17|False|Chị cảm giác có trách nhiệm phải kết thúc cuộc chiến này.

### P12

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|Làm sao?
Arra|2|True|Ừ, mày tin thực hành việc đó bằng cách nào?
Arra|3|False|Sức lực của mày chưa hồi phục, tao có thể cảm giác điều đó.
Hạt Tiêu|4|True|Các bạn rất chính xác, nhưng mình có một bài phép thuật có khả năng điều trị mỗi thứ.
Hạt Tiêu|5|False|Tôi chỉ cần đủ sức lực cho đến mỗi người.
Arra|6|True|Cho một phù thủy sức lực?
Arra|7|True|Điều đó bị cấm!
Arra|8|False|Không thể nào!
Hạt Tiêu|9|False|Bạn thích coi họ sát hại nhau không?
Torreya|10|True|Làm ơn Ông A Ra. Chỉ lần này thôi. Các người và con rồng đang chiến đấu, họ là trường viên và gia định của chúng tôi.
Torreya|11|False|Và của ông nữa.
Thất Vị|12|False|Chân thành làm ơn Ông A Ra.
Arra|13|True|Hừ! Được!
Arra|14|False|Nhưng rủi ro của mày!

### P13

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|False|xỉ xỉ xỉ xỉ xỉ ! !
Hạt Tiêu|2|False|QUÁÁÁ!
Hạt Tiêu|3|False|Sức lực của con rồng, không thể tin tưởng
Thất Vị|4|False|Hạt Tiêu, lẹ đi! Cuộc chiến đấu!
Hạt Tiêu|5|True|Chiến... !
Hạt Tiêu|6|True|Hành... !
Hạt Tiêu|7|True|Biến hóa... !
Hạt Tiêu|8|True|Thành...
Hạt Tiêu|9|False|AÍ NHÂN
Sound|10|False|Dèèooo!!
Hạt Tiêu|11|False|Biến hóa

### P14

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|Phíít!
Sound|2|True|Dèèoo!
Sound|3|True|Phiuuu!
Sound|4|True|Xíííít!
Sound|5|True|Schsss!
Sound|6|True|Phíít!
Sound|7|True|Dèèoo!
Sound|8|True|Xííít!
Sound|9|True|Phiuuu!
Sound|10|False|Schsss!
Hạt Tiêu|11|True|Bài phép thuật này là phiên bản đầu tiên của chị để chống chiến tranh.
Hạt Tiêu|12|True|Nó biến đổi kẻ thù sang bạn bè...
Hạt Tiêu|13|False|….và bạo lực trở thành tình yêu, thiện lương, và nhân ái.
Thất Vị|14|True|Quau!
Thất Vị|15|True|Tốtl... rất hay Chị Hạt Tiêu!
Thất Vị|16|False|Hết người đều nghỉ đánh nhau!
Torreya|17|False|Nó đang hoạt động!

### P15

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Thất Vị|1|True|Những cái gì đang xảy ra?
Thất Vị|2|False|Vài người đang hôn nhau...?!
Torreya|3|True|Ủi... Rất nhiều cặp tình yêu mới!
Torreya|4|False|Chị Hạt Tiêu cố tinh làm như vậy hay không?
Hạt Tiêu|5|False|Thật không! Chị nghĩ là sức lực của con rồng tăng cường độ tình yêu trong bài phép thuật của chị.
Torreya|6|False|Hà hà, cuộc chiến này sẽ được ghi trực tiếp vào các cuốn sách lịch sử..
Thất Vị|7|True|Hỉ, hỉ,
Thất Vị|8|False|Nhất định!
Hạt Tiêu|9|False|Úúúú, xấu hổ qúa nhe!
Writing|10|False|- HẾT -

### P16

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|2021.12.15 Nghệ thuật & câu chuyện: David Revoy. Người kiểm tra kịch bản: Arlo James Barnes, Bhoren, Bobby Hiltz, Craig Maloney, Estefania de Vasconcellos Guimaraes, GunChleoc, Karl Ove Hufthammer, Nicolas Artance, Pierre-Antoine Angelini , Valvin. Phiên bản tiếng Việt: Dịch bởi: Hồ Nhựt Châu. Chỉnh sửa: Craig Maloney. Sáng chế từ vũ trụ Hà Nhất Hạ Người Sáng Tạo: David Revoy. Người bảo trì chánh: Craig Maloney. Tác giả: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Người chỉnh sửa: Willem Sonke, Moini, Hali, CGand, Alex Gryson Phần Mềm: Krita 5.0β, Inkscape 1.1 với Kubuntu Linux 20.04. Bản Phép: Creative Commons Attribution 4.0. www.peppercarrot.com
Hạt Tiêu|2|False|Bạn có biết không?
Hạt Tiêu|3|False|Hạt Tiêu & Củ Đỏ hoàn toàn tự do, nội dung mở và được tài trợ bởi đọc giả.
Hạt Tiêu|4|False|Cho tập này, chân thành cảm ơn %%%% hỗ trợ viên!
Hạt Tiêu|5|False|Bạn cũng có thể hỗ trợ Hạt Tiêu & Củ Đỏ và được ghi tên vào danh sách này!
Hạt Tiêu|6|False|Chúng tôi nhận Patreon, Tipeee, PayPal, Liberapay ...và còn nữa!
Hạt Tiêu|7|False|Xem www.peppercarrot.com cho thông tin thêm!
Hạt Tiêu|8|False|Cám ơn!
<hidden>|9|False|Bạn cũng có thể dịch trang này.
<hidden>|10|False|HƯỚNG DẪN CHO PHIÊN DỊCH VIÊN Thay thông tin của bạn vào đầy, ví dụ: Phiên Bản Tiếng Pháp Phiên Dịch: Tên Tôi. Kiểm tra: Tên Khác.
<hidden>|11|False|Những người kiểm tra kịch bản giúp phát triển câu chuyện, còn những người tìm sai lầm giúp chỉnh sửa văn bản.
