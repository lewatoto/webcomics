# Transcript of Pepper&Carrot Episode 37 [pl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Odcinek 37: Łzy feniksa

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Fiu,
Pepper|2|False|dobrze w końcu wysiąść.
Pepper|3|False|Ach, dzisiaj dzień targowy.
Pepper|4|False|Zjedzmy coś, zanim zaczniemy wspinaczkę na wulkan.
Pepper|5|False|Wiedziałam, że będziesz za.

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Writing|1|False|POSZUKIWANI
Writing|2|False|Torreya
Writing|3|False|1 00000 Ko
Writing|4|False|Shichimi
Writing|5|False|250000 Ko
Writing|6|False|Pepper
Writing|7|False|1000000 Ko

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Porozwieszali je wszędzie.
Pepper|2|False|Nawet z dala od cywilizacji.
Pepper|3|False|Zabierajmy się stąd, zanim nas rozpoznają.
Pepper|4|False|Jeszcze tego nam brakowało.
Pepper|5|False|To światło...
Pepper|6|False|Musimy być blisko gniazda.
Pepper|7|False|Bingo!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Phoenix|1|False|Dlaczego zakłócasz mój spoczynek, człowieku?
Pepper|2|False|Witaj, o wielki feniksie.
Pepper|3|False|Mam na imię Pepper i jestem wiedźmą Chaosu.
Pepper|4|False|Niedawno przyjęłam dużą dawkę smoczej Rei, po czym na mojej skórze wyrosło to. Powiększa się każdego dnia.
Pepper|5|False|Niweluje moje moce, a w końcu może mnie nawet zabić.

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Phoenix|1|True|Hmm,
Phoenix|2|False|rozumiem.
Pepper|3|False|Zatem jesteś tutaj, ponieważ potrzebujesz mojej łzy do wyleczenia, tak?
Pepper|4|False|Tak, to moje je-dyne rozwiązanie.
Phoenix|5|False|Ech...
Phoenix|6|False|No na co czekasz?
Phoenix|7|False|Doprowadź mnie do łez.
Phoenix|8|False|Masz na to minutę!
Pepper|9|False|Że co?!
Pepper|10|False|Doprowadzić cię do łez?!
Pepper|11|False|Ale nie mam pojęcia, jak.
Pepper|12|False|No i... tylko jedna minuta?!
Pepper|13|True|Uch!
Pepper|14|True|OK!
Pepper|15|False|Pomyślmy.
Pepper|16|True|Wyobraź sobie głód na świecie.
Pepper|17|True|A, nienienienie!
Pepper|18|True|Mam coś lepszego!
Pepper|19|False|Przypomnij sobie tych, których straciłeś.
Pepper|20|True|Nic?
Pepper|21|True|Porzucone szczeniaczki?
Pepper|22|False|To przecież takie smutne...
Phoenix|23|False|...

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|I co ty na to?
Pepper|2|False|Nie uroniłeś ani łezki?
Phoenix|3|False|JESTEŚ POWAŻNA?!
Phoenix|4|False|TYLKO NA TYLE CIĘ STAĆ?!
Phoenix|5|True|Inni próbowali poezji!
Phoenix|6|True|Pisali tragedie!
Phoenix|7|True|SZTUKA!
Phoenix|8|False|DRAMAT!
Phoenix|9|True|A TY CO?!
Phoenix|10|False|JESTEŚ NIEPRZYGOTOWANA!
Phoenix|11|True|TAK JEST, WYNOCHA!
Phoenix|12|False|ZWIEWAJ DO DOMU I PRZYJDŹ, JAK SIĘ PRZYGOTUJESZ!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Grr! Chodź, Pepper! Zaraz znajdziemy smutną historię.
Pepper|2|True|Dasz radę.
Pepper|3|False|DASZ RADĘ.
Pepper|4|True|Nie,
Pepper|5|False|nie uda się.
Vendor|6|False|Hej, ty! Uciekaj!
Pepper|7|False|!!!
Vendor|8|False|Albo płacisz, albo zabieraj pazury!
Pepper|9|True|No nie.
Pepper|10|True|CARROT!
Pepper|11|False|Mógłbyś mi pomóc, zamiast próbować się najeść!
Pepper|12|False|Hmm?!
Pepper|13|True|Taaaak,
Pepper|14|False|to może zadziałać.

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Phoenix|1|True|Wróciłaś!
Phoenix|2|False|Jakoś prędko.
Phoenix|3|False|Żelazny miecz?
Phoenix|4|False|Doprawdy?!
Phoenix|5|False|Czyżbyś nie wiedziała, że stopię każdy metal?
Sound|6|False|Pac
Sound|7|False|Pac
Sound|8|False|Pac
Phoenix|9|True|O NIE!
Phoenix|10|False|TYLKO NIE TO!
Phoenix|11|False|TO NIE FAIR!
Pepper|12|True|Szybko, Carrot!
Pepper|13|False|Nałap tych łez, ile tylko możesz!
Sound|14|False|Ciach!
Sound|15|False|Ciach!
Sound|16|False|Ciach!
Title|17|False|- KONIEC -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|3 sierpnia 2022 Rysunki i scenariusz: David Revoy. Poprawki skryptu: Arlo James Barnes, Benjamin Loubet, Bobby Hiltz, Chloé, Craig Maloney, Estefania de Vasconcellos Guimaraes, GunChleoc, Nicolas Artance, Olivier Jolly, Rhombihexahedron, Valvin. Wersja polska Tłumaczenie: Sölve Svartskogen. Korekta i kontrola jakości: Besamir. Oparto na uniwersum Herevy Autor: David Revoy. Pomocnik: Craig Maloney. Pisarze: Craig Maloney, Nartance, Scribblemaniac, Valvin. Korekta: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita 5.0.5, Inkscape 1.2 na Fedora 36 KDE Spin. Licencja: Creative Commons Uznanie Autorstwa 4.0. www.peppercarrot.com
Pepper|2|False|Wiedziałeś, że
Pepper|3|False|Pepper&Carrot jest całkowicie darmowy, liberalny, open-source'owy oraz wspierany przez naszych czytelników?
Pepper|4|False|Za ten odcinek dziękujemy 1058 patronom!
Pepper|5|False|Ty również możesz zostać patronem Pepper&Carrot i znaleźć się na tej liście.
Pepper|6|False|Jesteśmy na Patreonie, Tipeee, PayPal, Liberapay i wielu innych!
Pepper|7|False|Wejdź na www.peppercarrot.com, by dowiedzieć się więcej!
Pepper|8|False|Dziękujemy!
Notes|9|False|You can also translate this page if you want.
Notes|10|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
Notes|11|False|Beta readers help with the story, proofreaders give feedback about the text.
