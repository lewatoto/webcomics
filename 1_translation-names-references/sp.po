#: ~~~~~~ NOTICE: The primary purpose of this file is to help your translations
#: be consistent across episodes, and as a guide when multiple translators work
#: on the same language. secondary for generating transcripts. Comments
#: starting by #: are coming from _catalog.pot file, will always overwritten if
#: you edit them. To write own comments, please start them simply with # (no
#: colon) they'll stay around. You can read more about PO reference files in
#: translation documentation repository.
msgid ""
msgstr ""
"Language: sitelen pona\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Translator: Name <email@adress.com>\n"

#: ~~~~~~ TRANSCRIPT SPECIFICS: Title of the episode
msgid "Title"
msgstr "nimi"

#: Used for the sound effects ('SFX', onomatopoeia)
msgid "Sound"
msgstr "a>__kalama"

#: For anything written on a wall, sign, paper, etc.
msgid "Writing"
msgstr "sitelen"

#: For speech bubbles of the narrator
msgid "Narrator"
msgstr "sitelen+toki"

#: For notes appearing under panels
msgid "Note"
msgstr "sitelen"

#: For the credits at end of each episode.
msgid "Credits"
msgstr "mama"

#: ~~~~~~ CHARACTERS (Episode 1)
msgid "Pepper"
msgstr "jan+namako [_pana+e_pona+ante]"

#: How the 'Pepper' & 'Carrot' words are connected in title -- for reference
#: only, not used by transcripts.
msgid "&"
msgstr "en"

msgid "Carrot"
msgstr "soweli [_kili+anpa_wile+awen]"

#: (Episode 3, Episode 37) A male vendor at the market.
msgid "Vendor"
msgstr "jan+esun"

#: A red-haired witch wearing luxurious jewels.
msgid "(Miss) Saffron"
msgstr "jan+namako [_seli+ale_pi1+olin_nasa]"

#: Saffron's pet, a white female cat.
msgid "Truffel"
msgstr "soweli [_tawa+utala_pana+esun]"

#: (Episode 6) A witch with curly black hair who is a princess and later queen.
msgid "Coriander"
msgstr "jan+namako [_kepeken+olin_wile+ilo_jan+a_nasin+ante]"

#: Coriander's pet, a male black cockerel.
msgid "Mango"
msgstr "waso [_mu+ala_nasin_kon+olin]"

#: A witch: blonde, with pink- or red-irised eyes, in a kimono.
msgid "Shichimi"
msgstr "jan+namako [_suno+ike_sewi+ilo_mun+insa]"

#: Shichimi's pet, a male fox with multiple tails.
msgid "Yuzu"
msgstr "soweli [_jelo_uta_suwi+uta]"

#: Also known as Mayor Bramble, he is a man with 'van Dyke' beard, brown suit
#: and hat; presents the potion magic contests.
msgid "Mayor of Komona"
msgstr "jan+lawa pi+__ma [_kasi+ona_mani+o_nasin+ale]"

#: Generic term for audiences.
msgid "Audience"
msgstr "a>__kalama_jan"

#: Generic term for any bird.
msgid "Bird"
msgstr "waso"

#: (Episode 7) Generic term for any fairy.
msgid "Fairies"
msgstr "jan+sewi lili"

#: (Episode 8) Generic term for any monster.
msgid "Monster"
msgstr "kon ike"

#: (Episode 11) The bon-vivant or happy-go-lucky witch who is one of Pepper's
#: three godmothers.
msgid "Cumin"
msgstr "jan+namako [_kama+uta_musi+insa_nasa]"

#: The tall, thin, and behaviourally-rigid witch who is one of Pepper's three
#: godmothers.
msgid "Cayenne"
msgstr "jan+namako [_kiwen+awen_jan+en_nasa]"

#: The ancient, small, and wise leader witch who is one of Pepper's three
#: godmothers.
msgid "Thyme"
msgstr "jan+namako [_toki+uta_mute+esun]"

#: Name of the young blond haired prince (as-yet-uncrowned king) Acren.
msgid "(Prince) Acren"
msgstr "ma [_awen_kiwen+e_ni]"

#: Generic term used by Pepper to refer the three witches who raised her.
msgid "godmothers"
msgstr "jan+sona"

#: (Episode 18) Master and teacher of Hippiah, an elf with blond curly hair.
msgid "Basilic"
msgstr "<TODO>"

#: A student witch of Hippiah, with long dark hair.
msgid "Oregano"
msgstr "<TODO>"

#: A student witch of Hippiah, with short blond hair and freckles.
msgid "Cardamom"
msgstr "<TODO>"

#: A student witch of Hippiah and also an elf, with red hair.
msgid "Cinnamon"
msgstr "<TODO>"

#: The main recurrent witch of Hippiah, a human with raccoon ears and tail.
msgid "Camomile"
msgstr "<TODO>"

#: (Episode 21) A witch of Aquah, living in water, with a long white hair
#: crest.
msgid "Spirulina"
msgstr "<TODO>"

#: Spirulina's pet, a male _Betta splendens_ fish.
msgid "Durian"
msgstr "<TODO>"

#: (Episode 22) One of the jury members, he wears a white beard, monocle, and
#: wide-brimmed hat. His name references Frieza from Dragon Ball.
msgid "Lord Azeirf"
msgstr "<TODO>"

#: One of the jury members, she is a queen. Her name and hairstyle reference
#: Leia Organa from Star Wars.
msgid "Queen Aiel"
msgstr "<TODO>"

#: (Episode 23) The big and muscular golden walrus genie.
msgid "Genie of Success"
msgstr "kon sewi pi+__pali__pona"

#: (Episode 27) The royal tailor of Coriander.
msgid "Tailor"
msgstr "jan pi+__pali__len"

#: The robotic invention of Coriander.
msgid "Psychologist-Bot"
msgstr "ilo pi+__sona+pilin"

#: (Episode 28) Generic term for any/all of the journalists asking questions at
#: coronation party.
msgid "Journalist"
msgstr "jan pi+__toki__pi1__ijo+sin"

#: (Episode 32) A bearded king wearing golden armour.
msgid "King"
msgstr "jan+lawa"

#: The officer of the bearded king, with a long blond mustache.
msgid "Officer"
msgstr "jan+pali pi+__jan+lawa"

#: Generic label used for all sounds produced by the armies.
msgid "Army"
msgstr "kulupu+utala"

#: (Episode 33) The name of the king with a hunting horn, leading opposing
#: ('dark') army
msgid "Enemy"
msgstr "jan+ike"

#: (Episode 34) A witch and knight of Ah, an elf from a desert. She is also the
#: teacher Shichimi.
msgid "Hibiscus"
msgstr "<TODO>"

#: The supreme leader witch of Ah, with green hair.
msgid "Wasabi"
msgstr "<TODO>"

#: (Episode 35) A witch of Ah who pilots a dragon, with spiky haircut and
#: aviator goggles. She is Shichimi's girlfriend.
msgid "Torreya"
msgstr "<TODO>"

#: Torreya's pet, a white dragon.
msgid "Arra"
msgstr "<TODO>"

#: (Episode 36) A rat appearing in the prison.
msgid "Rat"
msgstr ""

#: Generic terms for guards.
msgid "Guard"
msgstr ""

#: (Episode 37) The giant Phoenix
msgid "Phoenix"
msgstr ""

#: ~~~~~~ PLACES: (Episode 3) The flying city with the giant tree of Komona at
#: centre.
msgid "Komona City"
msgstr "ma [_kasi+ona_mani+o_nasin+ale]"

#: (Episode 6) The name of the village near Pepper's house.
msgid "Squirrel's End"
msgstr "ma[_pini+ijo_nena+insa] [_soweli+olin_wile+e_lupa+lili] pi+__kasi+mut"

#: A region of the world (Hereva) that federates many cities including ruled by
#: Coriander, Qualicity.
msgid "Technologist's Union"
msgstr "kulupu [_sona+olin _nasin+alasa ][_ilo_lon+ona]"

#: A large region of the world (Hereva) also known as land Ah, Shichimi's land.
msgid "the lands of the setting moons"
msgstr "ma pi+__mun+mute__anpa"

#: (Episode 16) The name of the planet and general setting Pepper&Carrot.
msgid "Hereva"
msgstr "<TODO>"

#: (Episode 27) Coriander's city, a large industrial city standing alone in the
#: middle of desert on rocky bluff.
msgid "Qualicity"
msgstr "ma [_tomo+open_meli+olin_pona+o_nanpa+ale]"

#: (Episode 31) A hill or small mountain sacred to the school of Chaosah.
msgid "Tenebrume"
msgstr "ma [_pakala-insa_ma+en_jan+ala_ma+awen]"

#: ~~~~~~ MAGIC: (Episode 8) The magic of chaos; that which Pepper now
#: practices.
msgid "Chaosah"
msgstr "kulupu [_pakala+ante_ken+ante_lawa+ante_a]"

#: (Episode 18) The magic of plants and living creatures; that which Camomile
#: practices Pepper used to.
msgid "Hippiah"
msgstr "kulupu [_pi1+ijo_pona+o_nasin+awen_a]"

#: (Episode 21) The magic of giving life to dead things including machines;
#: that which Coriander practices.
msgid "Zombiah"
msgstr "kulupu [_moli+o_lon+ilo_tawa+ante_wawa+ante_a]"

#: The magic of fire, melting metals and cooking; that which Saffron practices.
msgid "Magmah"
msgstr "kulupu [_seli+e_lili+insa_lon+ale_a]"

#: The magic of ghost and spirits; that which Shichimi practices.
msgid "Ah"
msgstr "kulupu [_awen_a]"

#: The magic of water, rain, and oceans; that which Spirulina practices.
msgid "Aquah"
msgstr "kulupu [_telo+en_lawa+o_ma+awen_a]"

#: (Episode 24) The substance or unit of magic, the name which is derived from
#: "reality".
msgid "Rea"
msgstr "wawa [_lon-open_nasa-awen_a]"

#: ~~~~~~ TIME SYSTEM: Weekday 1 − A day of recreation or rest, similar to
#: Sunday; associated with Chaosah, derived from the French word 'hazard'.
msgid "Azarday"
msgstr ""

#: Weekday 2 - associated with Magmah, from babka, a sweet brioche cake.
msgid "Babkaday"
msgstr "<TODO>"

#: Weekday 3 - associated with Aquah, from Ceto, a Greek mythological goddess
#: and sea monster.
msgid "Cetoday"
msgstr "<TODO>"

#: Weekday 4 - associated with Zombiah, from Donn, the lord of dead in Irish
#: myth.
msgid "Donday"
msgstr "<TODO>"

#: Weekday 5 - associated with Hippiah, egg = life.
msgid "Eggday"
msgstr "<TODO>"

#: Weekday 6 - associated with Ah, from the Japanese mythological god
#: Fukurokuju, fuku meaning "happiness".
msgid "Fookuday"
msgstr "<TODO>"

#: Weekday 7 - associated with all of the schools Hereva.
msgid "Zero's Day"
msgstr ""

#: Time - PM (afternoon)
msgid "Pinkmoon"
msgstr "tenpo+ilo [_pini+mun]"

#: Time - AM (morning)
msgid "Airmoon"
msgstr "tenpo [_anpa+mun]"

#: ~~~~~~ MISC: (Episode 3) A vegetable at the market, a sort of squash shaped
#: like yellow star. Used as an ingredient in potions.
msgid "pumpkinstar"
msgstr "kili+jelo"

#: The monetary unit of Komona city (and pun off 'kilo-octets', another word
#: for kilobytes).
msgid "Ko"
msgstr "mani [_kama+open_musi+o]"

#: A potion ingredient, obtained from clouds.
msgid "pearls of mist"
msgstr "sike telo lili"

#: A large, docile creature: half-dragon, half-cow.
msgid "DragonCow"
msgstr "waso akesi+soweli"

#: (Episode 14) A potion ingredient, a plant.
msgid "Dragon's Tooth"
msgstr "<TODO>"

#: Name for a kind of dragon, blue and flying at high altitude.
msgid "Air Dragon"
msgstr "akesi sewi+kon"

#: Name for a kind of dragon, covered with mud from the swamps.
msgid "Swamp Dragon"
msgstr "akesi pi+__ko+ma"

#: Name for a kind of dragon with body made electricity.
msgid "Lighting Dragon"
msgstr "akesi pi+__wawa+suno"

#: (Episode 21) Name of the newspaper published in Komona city and distributed
#: across Hereva.
msgid "The Komonan"
msgstr "lipu [_kasi+ona_mani+o_nasin+ale]"

#: (Episode 24) Name for the moonlit meeting of Chaosah big decisions and
#: exams.
msgid "council of The Three Moons"
msgstr "kulupu pi+__mun__tu+wan"

#: (Episode 26) Name for a small sacred tree in the bottom of an abandoned
#: castle, generating continuous stream water.
msgid "Water-Tree"
msgstr "kasi+telo"
